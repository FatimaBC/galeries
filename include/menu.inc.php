<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link href="css/style.css" rel="stylesheet" type="text/css">
   	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
	<script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js" ></script>
    <title>Accueil</title>
</head>
<body>
<?php include('functions.inc.php');?>
 	<nav class="menu row">
 		<a class="left col-md-2" href="index.php">
			<img src="img/logogalerie.png">
		</a>
		<div class="col-md-10">
			<ul class="align-right menu-infos">
		 		<li>A propos</li>
		 		<li>Plan du site</li>
	 		</ul>
	 		<ul class="align-center menu-items">
	 			<li><a href="photos.php">Toutes les photos</a></li>
		 <?php 
            foreach($lescategories as $index => $categorie)
            {
            	foreach ($categorie as $cat=>$themes)
               	{?>
           		 <li class="dropdown align-center">
            		<a class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" >
		            	<?= $cat;?>
		            	<span class="caret"></span>
	                </a>
	                <ul class="dropdown-menu">
            		<?php 
            		foreach ($themes as $theme)
            		{
            			?> <li class="align-center"><a href="view.php?theme=<?=$theme?>"><?=$theme?></a></li><?php 
            		}
            		?></ul>
            		</li>
           <?php  	}
            }?>     
			</ul>
		</div>
	</nav>
	<div class="content">
	   