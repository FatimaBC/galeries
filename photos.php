<?php include('include/menu.inc.php'); date_default_timezone_set('UTC'); ?>
<div class="index-block row">
	<div class="photos-block col-md-10 col-md-offset-1">
	 	<h1 class="index-block-title">Les photos</h1>
	 	<div class="noclass" id="noclass">
		</div>
		<div class="alert alert-info">
	      	<span class="glyphicon glyphicon-info-sign"></span>
	      	 Cliquez sur une photo pour la classer dans une catégorie.
	      </div>
	   <div class="nonclasseesPhotos row align-center "><!-- Photos non class�ess -->
	        <?php      	
	        	$arr=getPicturesNoCat();        		
	        	$noms=$arr[0];		
	        	for ($i=0;$i<count($noms);$i++)
	        	{
	       			echo'<a onclick="load('.$i.')">
	  						<img class="noclass-photo draggable" draggable="true" src="photos/'.$noms[$i].'">
	      				</a>';
	       		}  	?>
		</div>
	</div>
</div>
<?php include('include/footer.inc.php')?>
<script>        
function load(image)
{
	$('.noclass').load('chooseCat.php?img='+image);
	location.href="#noclass";
}
function loadChoose(img, cat, th)
{
	$('.noclass').load('chooseCat.php?img='+img+'&cat='+cat+'&th='+th);
	location.href="#noclass";
}
</script>