<?php include('include/menu.inc.php'); 

foreach($lescategories as $index_cat => $categorie)
{
	foreach ($categorie as $cat=>$themes)
	{
		for($num_th=0;$num_th<count($themes);$num_th++)
		{
			if (isset($_GET['theme']))
			{
				if($_GET['theme'] == $themes[$num_th]) //le th�me existe 
				{
					$photos=getPhotoByCatAndThem($index_cat,$num_th); //on va regarder s'il y a des photos dans le th�me $index=categorie et $i=theme
					if(empty($photos)) //Si il n'y en a  pas, un message s'affiche
					{
						echo 'Pas de photos dans cette cat�gorie.';
					}
					else{ //et si il existe des photos dans le th�me:
?>
<div class="row" style="margin:0;">
	<div class="index-block">
		<div class="wrapper-carousel">
		<!-- caroussel -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		    <!-- Indicators -->
		    <ol class="carousel-indicators">
		    <?php for ($x=0;$x<count($photos);$x++)//pour chaque photo
		    {
		    	if($x==0)
		    	{
		    		echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
		    	}
		    	else{
		    		echo'<li data-target="#myCarousel" data-slide-to="'.$x.'"></li>';
		    	}
			}?>
		    </ol>
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner" role="listbox">
		    <?php     
		    for ($j=0;$j<count($photos);$j++) //pour afficher chaque photo dans les slides
				{
					if($j==0) //premier slide (premi�re photo)
					{?>
						<div class="item active">
							<img src="photos/<?=$photos[$j]?>" alt="Chania" width="100%" height="345">
							<div class="carousel-caption">
					     	 	<h1><?=getTitre($photos[$j])?></h1>
					     	 	<h2>Mise en ligne le: <?=getDateCreation($photos[$j])?></h2>
					     	 	<h2>Mise à jour le : <?=getDateMaj($photos[$j], $num_th, $index_cat)?></h2>
					     	 </div>
						</div>
					<?php }
					else {?>
						<div class="item">
							<img src="photos/<?=$photos[$j]?>" alt="Chania" width="100%" height="345">
							<div class="carousel-caption">
					     	 	<h1><?=getTitre($photos[$j])?></h1>
					     	 	<h2>Mise en ligne le: <?=getDateCreation($photos[$j])?></h2>
					     	 	<h2>Mise à jour le : <?=getDateMaj($photos[$j], $num_th, $index_cat)?></h2>
					     	 </div>
						</div>
				<?php 	}
			 	}?>
		    <!-- Left and right controls -->
		    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		      <span class="sr-only">Next</span>
		    </a>
		  </div>
		</div>				
				<?php }
						}
					}
				}
			}
		}?>
		</div>
	</div>
</div>
<?php include('include/footer.inc.php')?>