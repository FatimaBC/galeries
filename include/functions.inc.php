<?php
$lescategories = array
(
		0=>array("Informatique"  => array("fonds d'écran","geek")),
		1=>array("Nature" => array("sauvage","fleur")),
);
function getTitre($img)
{
	$titre=null;
	$row=1;
	if(($handle=fopen('fichiers.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($data[0]==$img)
				{
					$titre=$data[1];
					break;
				}
			}
			$row++;
		}
		fclose($handle);
	}
	return $titre;
}
function ifExistsByCatTh($img, $cat, $theme)
{
	$exists=false;
	$row=1;
	if(($handle=fopen('photos.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($data[0]==$img && $data[4]==$cat && $data[5]==$theme)
				{
					$exists=true;
					break;
				}
			}
			$row++;
		}
		fclose($handle);
	}
	return $exists;
}
function ifExists($img)
{
	$exists=false;
	$row=1;
	if(($handle=fopen('fichiers.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($data[0]==$img)
				{
					$exists=true;
					break;
				}
			}
			$row++;
		}
		fclose($handle);
	}
	return $exists;
}
function getDateMaj($img,$th,$cat)
{
	$dateM=null;
	$row=1;
	if(($handle=fopen('photos.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($data[0]==$img && $data[4]==$cat && $data[5]==$th)
				{
					$dateM=$data[3];
					break;
				}
			}
			$row++;
		}
		fclose($handle);
	}

	return $dateM;
}
function getDateCreation($img)
{
	$dateC=null;
	$row=1;
	if(($handle=fopen('fichiers.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($data[0]==$img)
				{
					$dateC=$data[2];
					break;
				}
			}
			$row++;
		}
		fclose($handle);
	}
	
	return $dateC;
}
function addPicture()
{	
	$dossier = 'photos/'; //destination
	$fichier = basename($_FILES['file']['name']);//nom du fichier
	$exists=ifExists($fichier);//boolean 
	if($exists)//le fichier existe déjà en base
	{//on enregistre pas
		echo"<div class='alert alert-warning'><span class='glyphicon glyphicon-alert
	'></span>Vous avez déjà uploadé cette photo</div>";
	}
	else //le fichier n'existe pas en base
	{//on enregistre le fichier
		//verif si fichier est bien une image
		$extensions=array('.png', '.gif', '.jpg', '.jpeg','.PNG', '.JPG', '.GIF');
		$extension=strrchr($_FILES['file']['name'], '.');
		if(!in_array($extension, $extensions))
		{
			echo"<div class='alert alert-warning'><span class='glyphicon glyphicon-alert
	'></span>Veuillez uploader un fichier type image (gif, png, jpg, jpeg)</div>";
		}
		else{
			//renregistrement fichier
			if(move_uploaded_file($_FILES['file']['tmp_name'], $dossier . $fichier)) //enregistrement fichier dans le dossier destination
			{
				if(($handle=fopen('fichiers.csv','a')) !== FALSE)
				{
					$ligne=array($fichier,$_POST['titre'],$_POST['date'],'\n');
					fputcsv($handle, $ligne, ';');//enregistrement dans le fichier.csv
				}
				fclose($handle);
				echo '<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span>Image téléchargée avec succès.</div>';
			}
			else
			{
				echo'Erreur de téléchargement';
			}
		}
	}
}
function getPhotoByCatAndThem($cat,$theme)
{
	$noms=array();
	$row=1;
	if(($handle=fopen('photos.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				if($cat==$data[4] && $theme==$data[5])
				{
					array_push($noms,$data[0]);
				}
							}
			$row++;
		}
		fclose($handle);
	}
	return $noms;
}
function getPicturesNoCat()
{
	$arr=array();
	$noms=array();
	$dates=array();
	$row=1;
	if(($handle=fopen('fichiers.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				array_push($noms,$data[0]);
				array_push($dates,$data[1]);
			}
			$row++;
		}
		fclose($handle);
	}
	array_push($arr,$noms);
	array_push($arr,$dates);
	return $arr;
}
function  getPicturesNames()
{
	$arr=array();
	$row=1;
	//parcours du fichier photos.csv
	if(($handle=fopen('photos.csv','r')) !== FALSE)
	{
		while(($data=fgetcsv($handle,1000,';')) !==FALSE)
		{
			if($row!=1)
			{
				array_push($arr,$data[0]); //envoie dans l'array $arr le nom de chaque image uploadée
			}
			$row++; 
		}
		fclose($handle);
	}
	return $arr;
}
?>