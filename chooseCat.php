<?php
include('include/functions.inc.php');
$arr=getPicturesNoCat();//je récupère toutes les photos qui ont été uploadées
$noms=$arr[0];//tableau des noms des photos
if (isset($_GET['img']))
{
	$img=$_GET['img'];
?>
	<img src="photos/<?=$noms[$img]?>" class="col-md-3" alt="Image choisie">
<?php 
	$titre=getTitre($noms[$img]);
	$dateC=getDateCreation($noms[$img]);
?>
	<div class="">Titre: <?=$titre?><br/> Date de mise en ligne: <?=$dateC?></div>
	<p class="choose-cat-title col-md-3" >Choisissez une catégorie:</p>
	<div class="row">
	 <?php foreach($lescategories as $index => $categorie)
        {     
        	echo'<ul class="col-md-2 list">';	
        	foreach ($categorie as $cat=>$themes)
        	{?>
				<li><?=$cat?>
					<ul>
				<?php 
				for($i=0;$i<count($themes);$i++)
				{
					echo'<li class="listTh" onclick="loadChoose('.$img.','.$index.','.$i.')">'.$themes[$i].'</li>';						
				}
			
				?>
      		 		</ul>
      		 	</li>
      <?php }
        	echo' </ul>';
        }
        echo'</div>';
}
if (isset($_GET['img']) && isset($_GET['cat']) && isset($_GET['th']))
{	
	$imgname=$noms[$_GET['img']];
	$cat=$_GET['cat'];
	$th=$_GET['th'];
	$titre=getTitre($imgname);
	$exists=ifExistsByCatTh($imgname, $cat, $th);
	if($exists==true)
	{
		echo'
			<div class="alert alert-warning">
      			La photo existe déjà dans cette catégorie.
      		</div>
			';
	}
	else
	{
		if(($handle=fopen('photos.csv','a')) !== FALSE)
		{
			$dateCreation=getDateCreation($imgname);
			$ligne=array($imgname,$titre,$dateCreation,date('d-m-Y h:i:s'),$cat,$th,'\n');
			fputcsv($handle, $ligne, ';');
		}
		fclose($handle);?>
<div class="modal fadeIn" id="myModal" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
	<div class="modal-content">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Succès!!</h4>
	    </div>
	    <div class="modal-body">
	        <p>Votre photo a bien été classée.
	    </div>
	    <div class="modal-footer">
	        <button type="button" data-dismiss="modal" class="close btn btn-default" data-dismiss="modal"><a onclick="location.reload();">Close</a></button>
	    </div>	
	</div>
	</div>
</div>
<script>
$( document ).ready(function() {
    $('#myModal').modal('show')  
});
</script>
	<?php 
}
}
?>