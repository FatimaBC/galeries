**DM1 de PHP Web - Remise à niveau:
**Galerie de photos 


# Pages #
* accueil -> menu 
* photos -> toutes les photos du site
* view -> photos d'un th�me

# Fonctionnalités abouties#
*Ajouter une photo
-> enregistrement de la photo dans le dossier photos
->enregistrement des informations de la photo dans le fichier fichiers.csv
-> si la photo existe d�j� en base, un message d'erreur appra�t
*Classer les photos
-> par cat�gorie->th�mes
-> enregistrement dans le fichier photos.csv
*Consultation des photos par th�mes
-> via un carroussel

# Fonctionnalit�s non abouties #
* Ajout/Suppression d'une galerie/th�me 
Il faudrait stocker les galeries/th�mes dans un fichier csv. 
*Sessions
Il faudrait ajouter un fichier user.csv et faire des sessions, du coup associer � chaque photo un user, et � chaque galerie un user. Pourquoi pas travailler dessus dans une prochaine version
*Ajouter un formulaire de contact
Il faudrait travailler sur les configurations du serveur web, et faire l'envoi d'email apr�s validation du formulaire via php
*On pourrait aussi ajouter un syst�me de tri dans la page photos.php
Gr�ce � du javascript