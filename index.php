 <?php include('include/menu.inc.php'); date_default_timezone_set('UTC');?>
 	<div class="entete"></div>
	<div class="index-block row">
		<div class="index-block-message align-center">
			<p>Bienvenue sur votre plateforme de stockage et d'organisation de vos photographies.</p>
			<p>Classez vos photos et emportez-les n'importe-où !</p>
		</div>
	<?php //ajout photo
	if (isset($_POST['submit-photo']))
	{	
			addPicture();
	}
	?>
		<div class="index-block-actions col-md-10 col-md-offset-1 row">
			<h1 class="index-block-title">Actions</h1>
			<div class="col-md-6">
				<h2 class="index-block-actions-title align-center" id="ajout_photo">Ajouter une photo</h2>
				<div class="index-block-actions-item" id="ajout_photo_div">
					<!-- formulaire ajout photo -->
					<form method="post" name="uploadF" action="#" enctype="multipart/form-data" class="uploadF">
			            <input type="hidden" name="date" value="<?= date('d/m/Y H:i:s')?>">
						<div class="row">	          
				            <label id="dropContainer" class="nameFile col-md-5 col-md-offset-4" for="inpufile">
				            	<span class="glyphicon glyphicon-download-alt"></span> <br/> Parcourrir ou faire glisser ici
				            </label>
				            <input class="file" type="file" name="file" id="inpufile" required="required">
				        </div>
			           	<div class="row">
			           	 	<label for="titre" class="col-md-2 col-md-offset-2" >Titre</label>
			            	<input type="text" name="titre" id="titre" class="col-md-5" required="required">
			            </div>
			            <input type="submit" class="btn btn-default" name="submit-photo">
		        	</form>
				</div> 
			</div>
			<div class="col-md-6">
				<h2 class="index-block-actions-title align-center" id="ajout_galerie">Ajouter une galerie</h2>
				<div class="index-block-actions-item" id="ajout_galerie_div">
					<form method="post">
						<label for="titre_galerie" class="align-right col-md-5">Nom de la galerie</label>
						<input type="text" class="col-md-5" name="titre_galerie" id="titre_galerie"  required="required">
						<input type="submit" class="btn btn-default" name="ajouter_galerie" value="Ajouter">
					</form>
				</div>
				<h2 class="index-block-actions-title align-center" id="ajout_theme">Ajouter un thème</h2>
				<div class="index-block-actions-item" id="ajout_theme_div">
					<form method="post">
						<label class="col-md-5 align-right" for="titre_theme">Nom du thème</label>
						<input type="text" class="col-md-5" name="titre_theme" id="titre_theme"  required="required">
						<input type="submit" class="btn btn-default" name="ajouter_theme" value="Ajouter">
					</form>
				</div>	
		</div>
	</div>
	</div>
<?php include('include/footer.inc.php')?>
<script>
//Pour afficher le nom du fichier lors de l'upload
function showName()
{
	var file=$('input.file');
	$('.nameFile').text(file.val());
}
$('.file').on('change', showName);
//Pour permettre le drag&drop dans l'upload fichier
dropContainer.ondragover = dropContainer.ondragenter = function(evt) {
	  evt.preventDefault();
	};
	dropContainer.ondrop = function(evt) {
	  // marche pas sur IE
	  inpufile.files = evt.dataTransfer.files;
	  evt.preventDefault();
	};		
</script>